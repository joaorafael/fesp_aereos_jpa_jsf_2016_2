package br.fesppr.ticketsAereos.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.fesppr.ticketsAereos.massaTestes.MassaTestesCiaAereaEnum;
import br.fesppr.ticketsAereos.model.CiaAerea;
import br.fesppr.ticketsAereos.modificadores.CiaAereaCreator;
import br.fesppr.ticketsAereos.verificador.CiaAereaVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da FESP
 * 
 * @author Mauda
 *
 */

@RunWith(Parameterized.class)
public class TesteCiaAereaModel extends AbstractCrudTestModel<CiaAerea, MassaTestesCiaAereaEnum> {
	
	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesCiaAereaEnum.getParameters();
	}	
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteCiaAereaModel(MassaTestesCiaAereaEnum ciaAereaEnum) {
		super(ciaAereaEnum);
		creator = CiaAereaCreator.getInstance();
		verificator = CiaAereaVerificator.getInstance();
		objectEnumTemp = MassaTestesCiaAereaEnum.getInstance();
	}	

		
}
