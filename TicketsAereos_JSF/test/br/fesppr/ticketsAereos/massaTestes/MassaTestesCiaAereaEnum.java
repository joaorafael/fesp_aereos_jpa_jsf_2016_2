package br.fesppr.ticketsAereos.massaTestes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public enum MassaTestesCiaAereaEnum {
	
	AZUL("Azul"),
	GOL("Gol");
	
	private String nome;
	
	private MassaTestesCiaAereaEnum(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public static MassaTestesCiaAereaEnum getInstance(){
		Random generator = new Random();
		MassaTestesCiaAereaEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesCiaAereaEnum[]> getParameters(){
		Collection<MassaTestesCiaAereaEnum[]> c = new ArrayList<MassaTestesCiaAereaEnum[]>();
		for(MassaTestesCiaAereaEnum ciaAerea : values()){
			c.add(new MassaTestesCiaAereaEnum[] { ciaAerea });
		}
		return c;
	}
	
	@Override
	public String toString() {
		return this.name();
	}	
}
