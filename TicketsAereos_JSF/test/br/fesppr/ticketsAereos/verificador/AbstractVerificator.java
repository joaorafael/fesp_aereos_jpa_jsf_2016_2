package br.fesppr.ticketsAereos.verificador;

import org.junit.Assert;

import br.fesppr.ticketsAereos.model.IdentifierInterface;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public abstract class AbstractVerificator<T extends IdentifierInterface, E extends Enum<E>> {
	
	public void verify(T object){
		// Verifica se os parametros nao sao nulos
		Assert.assertNotNull(object);
	}
	
	public void verify(T object, E enumm){
		// Verifica se os parametros nao sao nulos
		Assert.assertNotNull(object);
		Assert.assertNotNull(enumm);
	}
	
	public void verify(T objectBD, T object){
		// Verifica se os parametros nao sao nulos
		Assert.assertNotNull(objectBD);
		Assert.assertNotNull(object);
		
		//Isso eh possivel pela IdentifierInterface
		Assert.assertEquals(objectBD.getId(), object.getId());
	}
}
