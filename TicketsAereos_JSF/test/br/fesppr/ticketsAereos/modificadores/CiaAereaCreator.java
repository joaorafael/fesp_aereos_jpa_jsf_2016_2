package br.fesppr.ticketsAereos.modificadores;

import br.fesppr.ticketsAereos.massaTestes.MassaTestesCiaAereaEnum;
import br.fesppr.ticketsAereos.model.CiaAerea;

public class CiaAereaCreator extends AbstractCreator<CiaAerea, MassaTestesCiaAereaEnum> {

	private static CiaAereaCreator instance = new CiaAereaCreator();
	
	private CiaAereaCreator() {	}
	
	@Override
	public CiaAerea createBlank(){
		return new CiaAerea();
	}
	
	@Override
	public CiaAerea create(MassaTestesCiaAereaEnum enumm) {
		// Cria o endereco
		CiaAerea ciaAerea = this.createBlank();

		// Atualiza as informacoes de acordo com o enum
		this.update(ciaAerea, enumm);

		// Retorna a CiaAerea
		return ciaAerea;
	}

	@Override
	public void update(CiaAerea ciaAerea, MassaTestesCiaAereaEnum enumm) {
		super.update(ciaAerea, enumm);
		
		ciaAerea.setNome(enumm.getNome());
	}
	
	@Override
	public void update(CiaAerea ciaAerea, String codigo) {
		ciaAerea.setNome(codigo + ciaAerea.getNome());
	}
	
	public static CiaAereaCreator getInstance() {
		return instance;
	}
}
