package br.fesppr.ticketsAereos.modificadores;

import org.junit.Assert;

import br.fesppr.ticketsAereos.model.IdentifierInterface;

/**
 * Classe responsavel por criar instancias de classes a partir de enums
 * @author Mauda
 *
 */
public abstract class AbstractCreator<T extends IdentifierInterface, E extends Enum<E>> {
	
	public abstract T createBlank();
	
	public abstract T create(E e);
	
	public void update(T t, E e){
		// Verifica se os parametros nao sao nulos
		Assert.assertNotNull(t);
		Assert.assertNotNull(e);	
	}
	
	public abstract void update(T t, String s);
}
