package br.fesppr.ticketsAereos.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Classe Utilitaria para tratamento de datas
 * @author Mauda
 *
 */

public class DateUtils {
	
	public enum DateFormat {
		USA_FORMAT("MM/dd/yyyy"),
		BR_FORMAT("dd/MM/yyyy");
		
		private String format;

		private DateFormat(String format) {
			this.format = format;
		}
		
		public String getFormat() {
			return format;
		}
	}
		
	public static Date parse(String date, DateFormat formato) throws ParseException {
		if(date == null){
			throw new ParseException("Devera ser informada uma data a ser formatada", 0);
		}
		if(formato == null){
			throw new ParseException("Devera ser informado um formato de data para a formatacao", 0);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(formato.getFormat());
		return sdf.parse(date);
	}
	
	public static String parse(Date date, DateFormat formato) throws ParseException {
		if(date == null){
			throw new ParseException("Devera ser informada uma data a ser formatada", 0);
		}
		if(formato == null){
			throw new ParseException("Devera ser informado um formato de data para a formatacao", 0);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(formato.getFormat());
		return sdf.format(date);
	}
	
	
	public static Date minimizeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static Date maxmizeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		// o milesegundos eh 998, porque o banco de dados faz confusao quando passa 999 e passa para o dia seguinte.
		// nao eh um erro de implementacao, mas um erro do java e do banco de dados.
		calendar.set(Calendar.MILLISECOND, 998);

		return calendar.getTime();
	}
	
	/**
	 * Metodo que retorna a data corrente mais plusDays dias.
	 * Ex: currentDatePlusDays(1) => Dia de hoje mais 24 horas
	 * @param plusDays
	 * @return
	 */
	public static Date currentDatePlusDays(int plusDays) {
		return datePlusDays(new Date(), plusDays);
	}
	
	/**
	 * Metodo que retorna a data corrente mais plusDays dias.
	 * Ex: datePlusDays(1) => date mais 24 horas
	 * @param plusDays
	 * @return
	 */
	public static Date datePlusDays(Date date, int plusDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, plusDays);
		return calendar.getTime();
	}
	
	/**
	 * Metodo que retorna a data corrente mais plusDays dias.
	 * Ex: datePlusHours(date, 1) => date mais 1 hora
	 * @param plusDays
	 * @return
	 */
	public static Date datePlusHours(Date date, int plusHours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, plusHours);
		return calendar.getTime();
	}
}
