package br.fesppr.ticketsAereos.view.mb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Everson Mauda
 * @version 1.0.0
 */

@ManagedBean
@ViewScoped
public class PesquisarCiaAereaMB implements Serializable{
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;
	
	public static final String CIAAEREA_SELECIONADA = "ciaAereaSelecionada";
	public static final String FILTRO_PESQUISA = "filtroPesquisa";
	
	//Utilizado para logs via Log4J
	private static Logger log = LogManager.getLogger(PesquisarCiaAereaMB.class);
	
	//TODO criar atributo para o filtro de pesquisa
	//TODO criar atributo para lista de retorno da pesquisa
	//TODO criar atributo para a cia aerea selecionada para edicao/exclusao/validacao
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public PesquisarCiaAereaMB() {
	}
	
	@PostConstruct
	private void init(){
		//TODO obter o FILTRO_PESQUISA da ViewUtil
		//TODO se o filtro de pesquisa eh nulo
			//TODO inicializar o filtro de pesquisa
		//TODO chama o metodo pesquisar
	}
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	//Action de Pesquisar a partir do filtro
	public void pesquisar() {
		//TODO chamar o metodo findByFiltro da classe BC correspondente
		//TODO se o retorno estiver vazio
			//TODO mostrar mensagem de nao ha resultados para a pesquisa correspondente
	}
	
	public String editar(){
		//TODO chamar o metodo validate()
		//TODO seta os parametros necess�rios para a edicao no ViewUtil
			//TODO CIAAEREA_SELECIONADA
			//TODO FILTRO_PESQUISA
			//TODO acao de editar
		//TODO retorna para a tela de manter
		return null;
	}
	
	public String excluir(){
		//TODO chamar o metodo validate()
		//TODO seta os parametros necess�rios para a exclusao no ViewUtil
			//TODO CIAAEREA_SELECIONADA
			//TODO FILTRO_PESQUISA
			//TODO acao de excluir
		//TODO retorna para a tela de manter
		return null;
	}
	
	public String visualizar(){
		//TODO chamar o metodo validate()
		//TODO seta os parametros necess�rios para a visualizacao no ViewUtil
			//TODO CIAAEREA_SELECIONADA
			//TODO FILTRO_PESQUISA
			//TODO acao de visualizar
		//TODO retorna para a tela de manter
		return null;
	}
	
	/////////////////////////////////////
	// Metodos Utilitarios
	/////////////////////////////////////
	
	private void validate(){
		//TODO se a cia selecionada for nula lanca exception
	}
	
	private void setParameters(ManterCiaAereaMB.Acoes acao){
		//TODO seta os parametros necess�rios para a acao no ViewUtil
		//TODO CIAAEREA_SELECIONADA
		//TODO FILTRO_PESQUISA
		//TODO acao de visualizar
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////
	
	//TODO criar get do filtro de pesquisa 
	//TODO criar get do retorno da pesquisa
	//TODO criar get e set da ciaAerea selecionada
}