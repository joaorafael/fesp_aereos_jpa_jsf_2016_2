package br.fesppr.ticketsAereos.view.mb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.fesppr.ticketsAereos.view.messages.MessagesUtils;

/**
 * @author Everson Mauda
 * @version 1.0.0
 */

@ManagedBean
@ViewScoped
public class ManterCiaAereaMB implements Serializable{
	
	public enum Acoes {
		EDITAR, EXCLUIR, INCLUIR, VISUALIZAR;
	}
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;
	
	//Utilizado para logs via Log4J
	private static Logger log = LogManager.getLogger(ManterCiaAereaMB.class);
	
	//TODO criar atributo para o formulario de cadastro
	//TODO criar atributo para o filtro de pesquisa
	
	private Acoes acao = Acoes.INCLUIR;
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public ManterCiaAereaMB() {
	}
	
	@PostConstruct
	private void init(){
		//TODO obter o parametro da classe Acoes da ViewUtil
		//TODO se a acao n�o for de nula nem de INCLUIR
			//TODO obter a CIAAEREA_SELECIONADA da ViewUtil
			//TODO validar se a CIA AEREA nao eh nula
			//TODO obter o FILTRO_PESQUISA da ViewUtil
		//TODO senao seta a acao como INCLUIR
	}	
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	//Action de Salvar Usuario
	public String salvar() {
		//TODO Chama o metodo da classe BC correspondente a acao atual da tela
			//TODO incluir = insert
			//TODO editar = update
			//TODO excluir = delete
		//TODO adicionar mensagem de sucesso
		//TODO seta o metodo de pesquisa
		//TODO retorna para a tela de pesquisar
		return null;
	}
	
	public String voltar(){
		//TODO seta o metodo de pesquisa
		//TODO retorna para a tela de pesquisar
		return null;
	}
	
	//Action para reset do cadastro
	public void limpar() {
		//TODO inicializar o atributo para o formulario de cadastro
	}
	
	/////////////////////////////////////
	// Metodos Utilitarios
	/////////////////////////////////////
	
	public boolean isAcaoEditar(){
		return Acoes.EDITAR.equals(acao);
	}
	
	public boolean isAcaoExcluir(){
		return Acoes.EXCLUIR.equals(acao);
	}	
	
	public boolean isAcaoIncluir(){
		return Acoes.INCLUIR.equals(acao);
	}

	public boolean isAcaoVisualizar(){
		return Acoes.VISUALIZAR.equals(acao);
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////
	
	//TODO criar get e set para o formulario de cadastro
	
	public void setAcao(Acoes acao) {
		this.acao = acao;
	}
	
	public String getTitle(){
		switch(acao){
			case EDITAR:
				return MessagesUtils.getLabel("editarCiaAerea");
			case EXCLUIR:
				return MessagesUtils.getLabel("excluirCiaAerea");
			case INCLUIR:
				return MessagesUtils.getLabel("incluirCiaAerea");
			case VISUALIZAR:
				return MessagesUtils.getLabel("visualizarCiaAerea");
		}
		return null;
	}
}