package br.fesppr.ticketsAereos.view.messages;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class BSIExceptionHandlerFactory extends ExceptionHandlerFactory {
	private ExceptionHandlerFactory parent;

	// this injection handles jsf
	public BSIExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		this.parent = parent;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {

		ExceptionHandler handler = new BSIExceptionHandler(
				parent.getExceptionHandler());

		return handler;
	}

}
