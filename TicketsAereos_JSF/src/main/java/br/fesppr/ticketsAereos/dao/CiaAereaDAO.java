package br.fesppr.ticketsAereos.dao;

import java.util.Collection;

import br.fesppr.ticketsAereos.dto.CiaAereaDTO;
import br.fesppr.ticketsAereos.model.CiaAerea;

public class CiaAereaDAO extends PatternCrudDAO<CiaAerea, CiaAereaDTO> {

	private static final long serialVersionUID = -5023453157647854678L;

	//TODO Criar padrao Singleton

	private CiaAereaDAO() {
		super(CiaAerea.class);
	}

	@Override
	public Collection<CiaAerea> findByFilter(CiaAereaDTO filter) {
		//TODO obter a instancia de session do Hibernate
		//TODO criar o criteria
		//TODO setar o DISTINCT_ROOT_ENTITY
		//TODO criar as condicoes de acordo com o que for preenchido no filtro
		//TODO fechar todos os recursos utilizados
		//TODO substituir return abaixo
		return null;
	}
}
