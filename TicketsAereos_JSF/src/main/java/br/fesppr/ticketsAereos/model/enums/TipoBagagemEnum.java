package br.fesppr.ticketsAereos.model.enums;

public enum TipoBagagemEnum {

	MAO(1, "M", "M�o", 5.0), NACIONAL(2, "N", "Nacional",23.0), INTERNACIONAL(3, "I", "Internacional", 32.0);

	private int id;
	private String acronimo;
	private String nome;
	private double pesoMax;

	private TipoBagagemEnum(int id, String acronimo, String nome, double pesoMax) {
		this.id = id;
		this.acronimo = acronimo;
		this.nome = nome;
		this.pesoMax = pesoMax;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAcronimo() {
		return acronimo;
	}

	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPesoMax() {
		return pesoMax;
	}

	public void setPesoMax(double pesoMax) {
		this.pesoMax = pesoMax;
	}
}