package br.fesppr.ticketsAereos.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TB_CIA_AEREA")
public class CiaAerea implements IdentifierInterface, Serializable {
 
	private static final long serialVersionUID = 4500469532603777635L;
	
	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	private Long id;
	private String nome;
	
	private Set<Rota> rota;
	private Set<Funcionario> funcionario;
	
	public CiaAerea() {
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Set<Rota> getRota() {
		return rota;
	}

	public void setRota(Set<Rota> rota) {
		this.rota = rota;
	}

	public Set<Funcionario> getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Set<Funcionario> funcionario) {
		this.funcionario = funcionario;
	}

	@Override
	public String toString() {
		return "CiaAerea  [id=" + this.id + ", nome=" + this.nome + "]" ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.id ^ (this.id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		CiaAerea other = (CiaAerea) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}
}
 
