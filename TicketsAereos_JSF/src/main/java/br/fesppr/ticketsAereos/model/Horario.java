package br.fesppr.ticketsAereos.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "TB_HORARIO")
public class Horario implements IdentifierInterface, Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String codigo;
	private Date partida;
	private Date chegada;
	private int qtdeEconomica;
	private int qtdePrimeira;
	private int qtdeExecutiva;

	private Rota rota;
	
	private List<Horario> economica;
	private List<Horario> executiva;
	private List<Horario> primeira;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getPartida() {
		return partida;
	}

	public void setPartida(Date partida) {
		this.partida = partida;
	}

	public Date getChegada() {
		return chegada;
	}

	public void setChegada(Date chegada) {
		this.chegada = chegada;
	}

	public int getQtdeEconomica() {
		return qtdeEconomica;
	}

	public void setQtdeEconomica(int qtdeEconomica) {
		this.qtdeEconomica = qtdeEconomica;
	}

	public int getQtdePrimeira() {
		return qtdePrimeira;
	}

	public void setQtdePrimeira(int qtdePrimeira) {
		this.qtdePrimeira = qtdePrimeira;
	}

	public int getQtdeExecutiva() {
		return qtdeExecutiva;
	}

	public void setQtdeExecutiva(int qtdeExecutiva) {
		this.qtdeExecutiva = qtdeExecutiva;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public List<Horario> getEconomica() {
		return economica;
	}

	public void setEconomica(List<Horario> economica) {
		this.economica = economica;
	}

	public List<Horario> getExecutiva() {
		return executiva;
	}

	public void setExecutiva(List<Horario> executiva) {
		this.executiva = executiva;
	}

	public List<Horario> getPrimeira() {
		return primeira;
	}

	public void setPrimeira(List<Horario> primeira) {
		this.primeira = primeira;
	}
}