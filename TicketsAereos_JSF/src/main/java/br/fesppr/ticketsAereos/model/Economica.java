package br.fesppr.ticketsAereos.model;

@SuppressWarnings("serial")
public class Economica extends Bilhete {

	private Bagagem bagagem1;

	public Economica(Bagagem bagagem1) {
		this.bagagem1 = bagagem1;
	}

	@Override
	int getMaxBagagens() {
		return 1;
	}

	public Bagagem getBagagem1() {
		return bagagem1;
	}

	public void setBagagem1(Bagagem bagagem1) {
		this.bagagem1 = bagagem1;
	}
}