package br.fesppr.ticketsAereos.model.enums;

public enum TipoBibleteEnum {

	ECONOMICA(1, "EC", "Economica"), EXECUTIVA(2, "EX", "Executiva"), PRIMEIRA(3, "PR", "Primeira");

	private int id;
	private String nome;
	private String acronimo;

	private TipoBibleteEnum(int id, String acronimo, String nome) {
		this.id = id;
		this.acronimo = acronimo;
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAcronimo() {
		return acronimo;
	}

	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}
}