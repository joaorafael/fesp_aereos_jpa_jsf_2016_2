package br.fesppr.ticketsAereos.model;

@SuppressWarnings("serial")
public class Primeira extends Bilhete {

	private Bagagem bagagem1;
	private Bagagem bagagem2;
	private Bagagem bagagem3;

	public Primeira(Bagagem bagagem1, Bagagem bagagem2, Bagagem bagagem3) {
		this.bagagem1 = bagagem1;
		this.bagagem2 = bagagem2;
		this.bagagem3 = bagagem3;
	}

	@Override
	int getMaxBagagens() {
		return 3;
	}

	public Bagagem getBagagem1() {
		return bagagem1;
	}

	public void setBagagem1(Bagagem bagagem1) {
		this.bagagem1 = bagagem1;
	}

	public Bagagem getBagagem2() {
		return bagagem2;
	}

	public void setBagagem2(Bagagem bagagem2) {
		this.bagagem2 = bagagem2;
	}

	public Bagagem getBagagem3() {
		return bagagem3;
	}

	public void setBagagem3(Bagagem bagagem3) {
		this.bagagem3 = bagagem3;
	}
}