package br.fesppr.ticketsAereos.model.enums;

public enum SituacaoBibleteEnum {
	
	Disponivel(1, "Disponivel"), Reservado(2, "Reservado"), Comprado(3, "Comprado"), Checkin(4, "Ckeckin");
	
	private int id;
	private String nome;

	private SituacaoBibleteEnum(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}