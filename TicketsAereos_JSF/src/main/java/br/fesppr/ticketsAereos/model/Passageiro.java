package br.fesppr.ticketsAereos.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="TB_PASSAGEIRO")
public class Passageiro extends Pessoa implements Serializable {

	private String documento;
	private String numeroCartao;

	private List<Bilhete> bilhete;
	
	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	
	public List<Bilhete> getBilhete() {
		return bilhete;
	}

	public void setBilhete(List<Bilhete> bilhete) {
		this.bilhete = bilhete;
	}

	public Passageiro(Endereco endereco) {
		super(endereco);
	}
}