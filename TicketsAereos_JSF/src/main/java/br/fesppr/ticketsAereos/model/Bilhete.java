package br.fesppr.ticketsAereos.model;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.fesppr.ticketsAereos.model.Bagagem;
import br.fesppr.ticketsAereos.model.IdentifierInterface;
import br.fesppr.ticketsAereos.model.Passageiro;
import br.fesppr.ticketsAereos.model.enums.TipoBagagemEnum;

@SuppressWarnings("serial")
@Entity
@Table(name = "TB_BLHETE")
abstract class Bilhete implements IdentifierInterface, Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String assento;

	public Bilhete() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssento() {
		return assento;
	}

	public void setAssento(String assento) {
		this.assento = assento;
	}
	
	public void reservar(Passageiro passageiro, String assento) {
		
	}
	
	public void comprar() {
		
	}
	
	public void cancelarReserva() {
		
	}
	
	public Bagagem checkin(TipoBagagemEnum tipo, double peso) {
		return null;
	}
	
	abstract int getMaxBagagens();
}