package br.fesppr.ticketsAereos.model;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.fesppr.ticketsAereos.model.enums.TipoBagagemEnum;

@SuppressWarnings("serial")
public class Bagagem implements IdentifierInterface, Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private double peso;

	private TipoBagagemEnum tipoBagagem;
	
	private Economica economica;
	private Executiva executiva;
	private Primeira primeira;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public TipoBagagemEnum getTipoBagagem() {
		return tipoBagagem;
	}

	public void setTipoBagagem(TipoBagagemEnum tipoBagagem) {
		this.tipoBagagem = tipoBagagem;
	}

	public Economica getEconomica() {
		return economica;
	}

	public void setEconomica(Economica economica) {
		this.economica = economica;
	}

	public Executiva getExecutiva() {
		return executiva;
	}

	public void setExecutiva(Executiva executiva) {
		this.executiva = executiva;
	}

	public Primeira getPrimeira() {
		return primeira;
	}

	public void setPrimeira(Primeira primeira) {
		this.primeira = primeira;
	}
}