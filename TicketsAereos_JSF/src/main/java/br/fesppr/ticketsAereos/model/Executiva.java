package br.fesppr.ticketsAereos.model;

@SuppressWarnings("serial")
public class Executiva extends Bilhete {

	private Bagagem bagagem1;
	private Bagagem bagagem2;

	public Executiva(Bagagem bagagem1, Bagagem bagagem2) {
		this.bagagem1 = bagagem1;
		this.bagagem2 = bagagem2;
	}

	@Override
	int getMaxBagagens() {
		return 2;
	}

	public Bagagem getBagagem1() {
		return bagagem1;
	}

	public void setBagagem1(Bagagem bagagem1) {
		this.bagagem1 = bagagem1;
	}

	public Bagagem getBagagem2() {
		return bagagem2;
	}

	public void setBagagem2(Bagagem bagagem2) {
		this.bagagem2 = bagagem2;
	}
}