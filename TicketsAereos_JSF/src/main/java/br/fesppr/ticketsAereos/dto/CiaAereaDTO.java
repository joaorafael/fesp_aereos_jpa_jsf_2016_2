package br.fesppr.ticketsAereos.dto;


public class CiaAereaDTO {
 
	private String nome;

	public CiaAereaDTO() {
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
 
