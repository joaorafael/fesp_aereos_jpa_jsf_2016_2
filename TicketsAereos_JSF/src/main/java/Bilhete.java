import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.fesppr.ticketsAereos.model.Bagagem;
import br.fesppr.ticketsAereos.model.Horario;
import br.fesppr.ticketsAereos.model.IdentifierInterface;
import br.fesppr.ticketsAereos.model.Passageiro;
import br.fesppr.ticketsAereos.model.enums.SituacaoBibleteEnum;
import br.fesppr.ticketsAereos.model.enums.TipoBagagemEnum;
import br.fesppr.ticketsAereos.model.enums.TipoBibleteEnum;

@SuppressWarnings("serial")
@Entity
@Table(name = "TB_BLHETE")
abstract class Bilhete implements IdentifierInterface, Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String assento;

	private Horario economica;
	private Horario executiva;
	private Horario primeira;

	private SituacaoBibleteEnum situacaoBibleteEnum;
	private TipoBibleteEnum tipoBilheteBibleteEnum;
	private Passageiro passageiro;

	public Bilhete(Passageiro passageiro) {
		this.passageiro = passageiro;
	}

	public Bilhete() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssento() {
		return assento;
	}

	public void setAssento(String assento) {
		this.assento = assento;
	}

	public Horario getEconomica() {
		return economica;
	}

	public void setEconomica(Horario economica) {
		this.economica = economica;
	}

	public Horario getExecutiva() {
		return executiva;
	}

	public void setExecutiva(Horario executiva) {
		this.executiva = executiva;
	}

	public Horario getPrimeira() {
		return primeira;
	}

	public void setPrimeira(Horario primeira) {
		this.primeira = primeira;
	}

	public SituacaoBibleteEnum getSituacaoBibleteEnum() {
		return situacaoBibleteEnum;
	}

	public void setSituacaoBibleteEnum(SituacaoBibleteEnum situacaoBibleteEnum) {
		this.situacaoBibleteEnum = situacaoBibleteEnum;
	}

	public TipoBibleteEnum getTipoBilheteBibleteEnum() {
		return tipoBilheteBibleteEnum;
	}

	public void setTipoBilheteBibleteEnum(TipoBibleteEnum tipoBilheteBibleteEnum) {
		this.tipoBilheteBibleteEnum = tipoBilheteBibleteEnum;
	}

	public Passageiro getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}

	public void reservar(Passageiro passageiro, String assento) {

	}

	public void comprar() {

	}

	public void cancelarReserva() {

	}

	public Bagagem checkin(TipoBagagemEnum tipo, double peso) {
		return null;
	}

	abstract int getMaxBagagens();
}